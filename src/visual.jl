

"""
    getfgcolor(c)

returns either "#FFFFFF" (white) or "#000000" (black) given a color. handy
utility to find if "black" or "white" text would be appropriate given the background
color (c)

# Example:
```julia
julia> c=RGB{Float64}(0.1,0.9,0.9) |> getfgcolor
"#FFFFFF"

julia> c=RGB{Float64}(0.9,0.9,0.9) |> getfgcolor
"#000000"
```
"""
function getfgcolor(c::RGB{T}) where T <: Float64
    br = (c.r*0.299*255 + c.g*0.587*255 + c.b*0.114*255) 
    br > 186 ? "#000000" : "#FFFFFF"
end



##
## Commenting out in lieu of using PyPlot and Seaborn.jl
##
"""
    plot_confusion_matrix(z, labels)

this function uses `ColorSchemes.jl` and `PlotlyJS.jl` modules and plots 
a confusion matrix using heatmap construct. 

for colorscheme use any color scheme from the module ColorSchemes. Some good ones
I like are:
- :viridis
- :rainbow
- :tempo
- :southwest
- :rust
- :rose
- :avocado
- :beach
- :YlOrBr_[3-9]
- :RdBu_[3-10] ...
"""
# function  plot_confusion_matrix(z::Array, labels::Array; 
#                                 title="", 
#                                 round_annotation_text=2,
#                                 transform=true,
#                                 colorscheme=:viridis)

#     ndims(z) == 2 || error("Dimension of matrix is $(ndims(z)) expecting 2")

#     # transform the matrix such that origin is top left
#     # default Julia matrix are plotted with origin bottom left
#     dat = reverse(z', dims=2)

#     # Get colorscheme to use and derive min max values 
#     # for later calculating the bgcolor
#     # TODO: add an option to be able to use any colorscheme defined in ColorSchemes
#     #       this may require setting background color of each block
#     color_scheme = ColorSchemes.eval(colorscheme)
#     n_colors = length(color_scheme)
#     color_scale = zip(range(0,stop=1, length=n_colors), color_scheme) |> collect

#     _min,_max = minimum(dat),maximum(dat)
#     clrmin,clrmax = 1,length(color_scheme)
#     R = Rescale(_min:_max, clrmin:clrmax)


#     annot = []
#     for (ix, i) in enumerate(labels)
#         for (ij, j) in enumerate(labels)
#             bgidx = trunc(Int, encode(R, dat[ix, ij]))
#             bgcolor = color_scheme[bgidx]

#             # round off the value to ensure everything fits inside
#             # a small box
#             text = round(dat[ix, ij], digits=round_annotation_text)

#             r = attr(x=i, y=j, text=text,
#                      showarrow=false, xanchor="center", yanchor="false",
#                      font_color=getfgcolor(bgcolor)) # calc. if fgcolor should be white or black
#             push!(annot, r)
#         end
#     end

#     trace = heatmap(;x=labels, y=labels, z=dat, colorscale=color_scale) #colorscale="Viridis")
#     p = [trace]
#     l = Layout(;title=title, annotations=annot)
#     plt = plot(p, l)
# end