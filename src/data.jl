

_minmax(arr) = (minimum(arr), maximum(arr))
_between(rng::UnitRange, val) = ((val < minimum(rng)) || (val > maximum(rng))) ? false : true
function _between(arr::AbstractArray, val)
    _min,_max = _minmax(arr)
    rng = range(_min, stop=_max)
    _between(rng, val)
end


# 
# New Implementation of Rescale

"""
    Rescale(srclo, srchi, tgtlo, tgthi)
    Rescale(srclo:srchi, tgtlo:tgthi)

creates and object definition for rescaling values. Where
`srclo` & `srchi` defines the original scale and `tgtlo` & `tgthi`
defines the target scale where the values should be mapped to. 

corresponding functions `encode` and `decode` would then use
this object to rescale the values along these two scales.

# Example:

```julia
julia> R=Rescale(1:10, 1:256)
julia> encode(R, 2)
29.333333333333332

julia> decode(R, encode(R, 2))
2.0
```
"""
struct Rescale
    srclo
    srchi
    tgtlo
    tgthi
end

Broadcast.broadcastable(R::Rescale) = Ref(R)

function Rescale(src::UnitRange, tgt::UnitRange)
    Rescale(minimum(src), maximum(src), minimum(tgt), maximum(tgt))
end

function encode(R::Rescale, val)
    ((val < R.srclo) || (val > R.srchi)) && return("value $val not between $(R.srclo):$(R.srchi)")
    res = R.tgtlo + (val - R.srclo) * (R.tgthi - R.tgtlo) / (R.srchi - R.srclo)
end

function decode(R::Rescale, val)
    ((val < R.tgtlo) || (val > R.tgthi)) && return("value $val not between $(R.tgtlo):$(R.tgthi)")
    res = R.srclo + (val - R.tgtlo) * (R.srchi - R.srclo) / (R.tgthi - R.tgtlo)
end


"""
    rescale(fromrng, torng, v)

rescale value `v` from `fromrng` to `torng`

# Example:
```julia
julia> rescale(-10:20, 1:256, -2)
69.0
```
"""
function rescale(fromrng::Tuple, torng::Tuple, v)
    fromlow, fromhi = _minmax(fromrng)
    tolow, tohi = _minmax(torng)

    ((v < fromlow) || (v > fromhi)) && return("value $v not within range $fromrng")
    res = tolow + (v - fromlow) * (tohi - tolow) / (fromhi - fromlow)
end




unsqueeze(xs, dim) = reshape(xs, (size(xs)[1:dim-1]..., 1, size(xs)[dim:end]...))
"""
    stack(xs, dim)

stack a list of list into an matrix along dimension dim

# Example:
```julia 
julia> arr
10-element Array{Array{Int64,1},1}:
 [3, 4, 3, 2]
 [2, 2, 1, 1]
 [1, 2, 3, 2]
 [4, 3, 1, 1]
 [3, 2, 3, 1]
 [4, 3, 3, 1]
 [4, 2, 1, 3]
 [2, 4, 1, 2]
 [1, 1, 2, 2]
 [1, 2, 3, 4]

julia> stack(arr, 1)
10×4 Array{Int64,2}:
 3  4  3  2
 2  2  1  1
 1  2  3  2
 4  3  1  1
 3  2  3  1
 4  3  3  1
 4  2  1  3
 2  4  1  2
 1  1  2  2
 1  2  3  4

```
"""
stack(xs, dim) = cat(unsqueeze.(xs, dim)..., dims=dim)




"""
    unstack(xs, dim)

unstack a matrix into list of vectors along dimension dim

# Example:
```julia 
julia> z=rand(1:4, 10,4)
10×4 Array{Int64,2}:
 3  4  3  2
 2  2  1  1
 1  2  3  2
 4  3  1  1
 3  2  3  1
 4  3  3  1
 4  2  1  3
 2  4  1  2
 1  1  2  2
 1  2  3  4

julia> unstack(z, 1)
10-element Array{Array{Int64,1},1}:
 [3, 4, 3, 2]
 [2, 2, 1, 1]
 [1, 2, 3, 2]
 [4, 3, 1, 1]
 [3, 2, 3, 1]
 [4, 3, 3, 1]
 [4, 2, 1, 3]
 [2, 4, 1, 2]
 [1, 1, 2, 2]
 [1, 2, 3, 4]
```
"""
unstack(xs, dim) = [copy(selectdim(xs, dim, i)) for i in 1:size(xs, dim)]


"""
    topk(arr, k)

return index of the top `k` values in an array (default order is descending)
"""
function topk(arr::AbstractArray{T}, k::Integer; rev=true) where T<:Number
    (k <= length(arr) && k > 0) || error("Dimension mismatch.")

    kvarray = [(i,j) for (i,j) in enumerate(arr)]
    sortedarray = sort(kvarray, by=x->x[2], rev=rev)
    getindex.(sortedarray, 1)[1:k]
end