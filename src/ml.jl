

"""
    confusion_matrix(gts, preds)

given two vectors (`true_y` and `pred_y`) create a confusion
matrix.

```julia 
julia> l=["cat", "dog", "cow"];
julia> true_y = rand(l, 100);
julia> pred_y = rand(l, 100);

julia> confusion_matrix(true_y, pred_y)
3×3 Array{Int64,2}:
10   9  13
10   9  11
13  15  10

```

"""
function confusion_matrix(gts::Vector, preds::Vector)
    length(preds) == length(gts) || throw(DimensionMismatch("Inconsistent lengths."))

    gtslbl = sort(unique(gts))
    k = length(gtslbl)
    n = length(gts)

    lookup = Dict(reverse.(enumerate(gtslbl)|> collect))
    R = zeros(Int, k, k)
    for i = 1:n
        @inbounds g = lookup[gts[i]]
        @inbounds p = lookup[preds[i]]
        R[g, p] += 1
    end
    return R
end