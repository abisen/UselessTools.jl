module UselessTools

using ColorTypes
using ColorSchemes
#using PlotlyJS

include("data.jl")
export rescale,
       stack, 
       topk, 
       unstack,
       Rescale, 
       encode,
       decode

include("ml.jl")
export confusion_matrix
       
include("pipe.jl")
export @pipe


include("visual.jl")
export getfgcolor #,
       # plot_confusion_matrix



end # module